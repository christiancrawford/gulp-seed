// *************************************
//
//   Gulpfile
//   Western & Southern
//   Design Implementation Strategy
//   'version': '1.0.0'
//
// *************************************
//
// Available tasks:
//  'build:dev'
//  'build:prod'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  require-dir                  : Helper to require() directories.
//  run-sequence                 : Run a series of dependent gulp tasks in order
//
// -------------------------------------

const gulp = require('gulp');

// Import Modules
const requireDir = require('require-dir');
const runSequence = require('run-sequence');

// Pulling in all tasks from the tasks folder
requireDir('./gulp/tasks', {
    recurse: true
});

// -------------------------------------
//   Task: Build: Dev
// -------------------------------------
gulp.task('build:dev', () => {

    // Run one task at a time sequentially
    runSequence(
        // move all Css tmp folder
        'move:css',
        // lint Sass and report errors and warnings in console
        'lint:sass',
        // move all Assets to tmp directory
        'move:assets',
        // Compile all Sass in src and move to tmp
        'compile:sass',
        // Run W3C validation on Html and report errors and warnings in console
        'validate:html',
        // lint Bootstrap Html and report errors and warnings in console
        'lint:bootstrap',
        // move all JavaScript to tmp directory
        'move:javascript',
        // lint TypeScript and report errors and warnings in console
        'lint:typescript',
        // Compile all TypeScript in src and move to tmp
        'compile:typescript'
    )
});

// -------------------------------------
//   Task: Build: Prod
// -------------------------------------
gulp.task('build:prod', () => {

    // Run one task at a time sequentially
    runSequence(
        // move all JavaScript to the tmp directory
        'move:javascript',
        // move all Css to the tmp directory
        'move:css',
        // optimize (lossless) all images in the src directory and move to the prod directory
        'optimize:images',
        // minify all Css, compile Sass and move all Css
        // in the tmp directory to the prod directory
        'minify:css',
        // minify all JavaScript, compile TypeScript and move all JavaScript
        // in the tmp directory to the prod directory
        'minify:javascript'
    )
});

// -------------------------------------
//   Task: Run: Dev BrowserSync
// -------------------------------------
gulp.task('run:browsers', () => {

    // Run one task at a time sequentially
    runSequence(
        // Start local server on http://localhost:3000
        // should start automatically in your default browser.
        // The BrowserSync local setup can be reached at http://localhost:3001/.
        'sync:browsers'
    )
});
