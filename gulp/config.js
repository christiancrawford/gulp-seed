// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Configuration Setup

module.exports = {
    configs: {
        jslint: './gulp/config/.jshintrc',
        stylelint: './gulp/config/.stylelintrc',
        tslint: './gulp/config/tslint.json',
        typescript: './gulp/config/tsconfig.json'
    },
    paths: {
        project: './',
        assets: {
            entry: './src/assets/',
            all: './src/assets/**/*',
            dest: './prod/assets/'
        },
        css: {
            entry: './src/css/',
            vendor: './src/css/vendor/*.css',
            all: './src/css/**/*.css',
            dest: './prod/css/'
        },
        html: {
            entry: './',
            vendor: './src/vendor/*.html',
            all: './**/*.html',
            dest: './prod/'
        },
        js: {
            entry: './src/js/',
            vendor: './src/js/vendor/*.js',
            dest: './prod/js/',
            all: './src/js/**/*.js'
        },
        sass: {
            entry: './src/css/',
            vendor: './src/css/vendor/*.scss',
            all: './src/css/**/*.scss'
        },
        tmp: {
            entry: './.tmp/',
            assets: './.tmp/assets/',
            scripts: './.tmp/js/',
            styles: './.tmp/css/'
        },
        ts: {
            entry: './src/js/',
            vendor: './src/js/vendor/*.ts',
            all: './src/js/**/*.ts'
        },
        watch: {
            entry: './src/',
            assets: './src/assets/**/*',
            scripts: './src/js/**/*.js',
            styles: './src/css/**/*.css',
            html: './**/*.html'
        }
    },
    names: {
        css: {
            min: '.min.css',
            dest: 'prod.min.css',
            vendor: '.vendor.min.css'
        },
        js: {
            min: '.min.js',
            dest: 'prod.min.js',
            vendor: '.vendor.min.js'
        }
    }
};
