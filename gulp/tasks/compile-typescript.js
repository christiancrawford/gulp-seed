// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'compile:typescript'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-typescript              : Handle TypeScript compilation workflow
//  typescript                   : TypeScript support tools (needed by gulp-typescript)
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Compile: TypeScript, minifiy js,
//   minify js and move to destination folder
// -------------------------------------
gulp.task('compile:typescript', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const gulpTypescript = require('gulp-typescript');
    const tsProject = gulpTypescript.createProject(config.configs.typescript);

    return tsProject.src()

    // Compile TypeScript
    .pipe(tsProject())

    // Move js to new location
    // Trailing space in directory path
    // is neccessary for gulp to create
    // the directory folders for some reason.
    .pipe(gulp.dest(config.paths.tmp.scripts + ' '));
});
