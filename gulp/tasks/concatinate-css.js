// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'concatinate:css'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-concat-css              : Concatenates files
//
// -------------------------------------

const gulp = require('gulp');
const del = require('del');

// -------------------------------------
//   Task: Concatinate: JavaScript
// -------------------------------------
gulp.task('concatinate:css', () => {

    // Import config.css
    const config = require('../config');

    // Import Modules
    const concatinateCss = require('gulp-concat-css');

    return gulp.src(config.paths.css.dest + '**/*.css')
        .pipe(concatinateCss(config.names.css.dest))

        // Delete original files that are used in concatination
        // .on('end', function() {
        //     del(config.paths.css.dest + 'vendor'),
        //     del(config.paths.css.dest + '**/*.css')
        // })
        .pipe(gulp.dest(config.paths.css.dest));
});
