// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'validate:html'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-w3cjs                   : Test files or url's against the w3c html validator
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Validate: HTML
// -------------------------------------
// TODO: When do we validate if using cshtml?
gulp.task('validate:html', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const htmlValidator = require('gulp-w3cjs');

    gulp.src(config.paths.html.all)
        .pipe(htmlValidator());
});
