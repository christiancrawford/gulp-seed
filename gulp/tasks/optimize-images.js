// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'optimize:images'
//  'optimize:jpg-png-gif'
//  'optimize:svg'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-imagemin                : Minify images seamlessly
//  gulp-smushit                 : Optimize images. Made on top of smosh
//
// -------------------------------------

const gulp = require('gulp');

// Import config.js
const config = require('../config');

// -------------------------------------
//   Task: Run: optimize:jpg-png-gif, optimize:svg and move:pdf
// -------------------------------------
gulp.task('optimize:images', () => {

    // Import Modules
    const runSequence = require('run-sequence');

    // Run one task at a time sequentially
    runSequence(
        'optimize:svg',
        'optimize:jpg-png-gif'
    )
});

// -------------------------------------
//   Task: Optimize: Jpg, Png and Gif Images
// -------------------------------------
gulp.task('optimize:jpg-png-gif', () => {

    const smushit = require('gulp-smushit');

    return gulp.src([
            config.paths.assets.entry + '**/*.{jpg,png,gif,jpeg}'
        ])
        .pipe(smushit({

            // set up detailed reporting
            verbose: true
        }))
        .pipe(gulp.dest(config.paths.assets.dest));
});

// -------------------------------------
//   Task: Optimize: SVG Images
// -------------------------------------
gulp.task('optimize:svg', () => {

    const imagemin = require('gulp-imagemin');

    gulp.src(config.paths.assets.entry + '**/*.svg')
        .pipe(imagemin({

            // set up detailed reporting
            verbose: true
        }))
        .pipe(gulp.dest(config.paths.assets.dest));
});
