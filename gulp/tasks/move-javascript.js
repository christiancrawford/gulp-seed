// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'move:javascript'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Move: JavaScript
// -------------------------------------
gulp.task('move:javascript', () => {

    // Import config.js
    const config = require('../config');

    // Move plain JavaScript and already minified JavaScript
    // to the destination folder
    gulp.src([

            config.paths.js.entry + '**/*' + config.names.js.min,
            config.paths.js.all
        ])
        .pipe(gulp.dest(config.paths.tmp.scripts));
});
