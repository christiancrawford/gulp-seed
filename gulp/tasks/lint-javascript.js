// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'lint:javascript'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-jshint                  : JSHint plugin for gulp
//  jshint                       : Detect errors & potential problems in JavaScript code
//  jshint-visual-studio         : JSHint Visual Studio reporter
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Lint: JavaScript
// -------------------------------------
gulp.task('lint:javascript', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const jshint = require('gulp-jshint');
    const jshintVisualStudio = require('jshint-visual-studio');

    gulp.src(config.paths.js.all)
        .pipe(jshint(config.configs.jslint))
        .pipe(jshint.reporter(jshintVisualStudio));
});
