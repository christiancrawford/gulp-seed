// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'uglify:javascript'
//  'minify:javascript'
//  'move:javascript'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-rename                  : A gulp plugin to rename files easily.
//  run-sequence                 : Run a series of dependent gulp tasks in order
//  gulp-uglify                  : Minify files with UglifyJS
//
// -------------------------------------

const gulp = require('gulp');

// Import config.js
const config = require('../config');

// -------------------------------------
//   Task: Minify: JavaScript, Compile TypeScript and move JavaScript
//   that is already minified
// -------------------------------------
gulp.task('minify:javascript', ['compile:typescript'], () => {

    // Import Modules
    const runSequence = require('run-sequence');

    // Run one task at a time sequentially
    runSequence(
        'uglify:javascript',
        'move:minified-javascript'
    )
});

// -------------------------------------
//   Task: Uglify: JavaScript
// -------------------------------------
gulp.task('uglify:javascript', () => {

    // Import Modules
    const uglifyJavasScript = require('gulp-uglify');
    const rename = require('gulp-rename');

    return gulp.src([

        '!' + config.paths.js.entry + '**/*' + config.names.js.min,
        config.paths.js.entry + '**/*.js',
        '!' + config.paths.tmp.scripts + '**/*' + config.names.js.min,
        config.paths.tmp.scripts + '**/*.js'
    ])

    // Minify JavaScript
    .pipe(uglifyJavasScript())

    // Rename JavaScript
    .pipe(rename({
        extname: config.names.js.min
    }))

    // Move JavaScript to new location
    .pipe(gulp.dest(config.paths.js.dest));
});

// -------------------------------------
//   Task: Move: JavaScript
// -------------------------------------
gulp.task('move:minified-javascript', () => {

    // Import Modules
    const runSequence = require('run-sequence');

    // Move already minified JavaScript
    // to the destination folder
    gulp.src(config.paths.js.entry + '**/*' + config.names.js.min)
        .pipe(gulp.dest(config.paths.js.dest))
        .on('end', function() {
          runSequence(

              // Run task
              'concatinate:javascript'
          )
        });
});
