// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'concatinate:javascript'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-concat                  : Concatenates files
//
// -------------------------------------

const gulp = require('gulp');
const del = require('del');

// -------------------------------------
//   Task: Concatinate: JavaScript
// -------------------------------------
gulp.task('concatinate:javascript', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const concatinateJavascript = require('gulp-concat');

    return gulp.src(config.paths.js.dest + '**/*.js')
        .pipe(concatinateJavascript(config.names.js.dest))

        // Delete original files that are used in concatination
        .on('end', function() {
            del(config.paths.js.dest + 'vendor'),
            del(config.paths.js.dest + '**/*.js')
        })
        .pipe(gulp.dest(config.paths.js.dest));
});
