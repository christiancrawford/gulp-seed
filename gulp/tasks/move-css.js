// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'move:css'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Move: CSS
// -------------------------------------
gulp.task('move:css', () => {

    // Import config.js
    const config = require('../config');

    // Move plain CSS and already minified CSS
    // to the destination folder
    gulp.src([

            config.paths.css.entry + '**/*' + config.names.css.min,
            config.paths.css.all
        ])
        .pipe(gulp.dest(config.paths.tmp.styles));
});
