// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'sync:browsers'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  browser-sync                 : Keep multiple browsers & devices in sync
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Sync: Browsers
// -------------------------------------
// Use the local browsersync UI at http://localhost:3001/
gulp.task("sync:browsers", () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const browserSync = require("browser-sync").create();

    browserSync.init({
        server: {
            baseDir: ""
        }
    });

    // Watch for changes in these files on 'save'
    gulp.watch(config.paths.watch.assets).on("change", browserSync.reload);
    gulp.watch(config.paths.watch.scripts).on("change", browserSync.reload);
    gulp.watch(config.paths.watch.styles).on("change", browserSync.reload);
    gulp.watch(config.paths.watch.html).on("change", browserSync.reload);
});
