// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'lint:sass'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-stylelint               : Runs stylelint results through a list of reporters
//  stylelint-config-standard    : The standard shareable config for stylelint
//  stylelint-scss               : A collection of SCSS specific linting rules
//
// -------------------------------------

const gulp = require('gulp');

// Import config.js
const config = require('../config');

// -------------------------------------
//   Task: Lint: Sass
// -------------------------------------
gulp.task('lint:sass', () => {

    // Import Modules
    const stylelint = require('gulp-stylelint');
    const stylelintConfigStandard = require('stylelint-config-standard');
    const stylelintScss = require('stylelint-scss');

    return gulp.src([
            '!' + config.paths.sass.vendor,
            config.paths.sass.all
        ])
        .pipe(stylelint({
            configFile: config.configs.stylelint,
            failAfterError: false,
            reporters: [{
                formatter: 'string',
                console: true
            }]
        }));
});
