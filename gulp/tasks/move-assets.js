// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'move:assets'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Move: Assets
// -------------------------------------
gulp.task('move:assets', () => {

    // Import config.js
    const config = require('../config');

    gulp.src([
            config.paths.assets.all
        ])
        .pipe(gulp.dest(config.paths.tmp.assets));
});
