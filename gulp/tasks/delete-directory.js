// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'delete:directory'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  del                          : delete some files
//  vinyl-paths                  : when you need to use the file paths from a gulp pipeline in Promise-returning node module
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Delete: Directory
// -------------------------------------
gulp.task('delete:directory', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const del = require('del');
    const vinylPaths = require('vinyl-paths');

    return gulp.src([

        config.paths.tmp.entry
    ])

    // Delete folder/file
    .pipe(vinylPaths(del))
});
