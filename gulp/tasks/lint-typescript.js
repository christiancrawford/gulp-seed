// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'lint:typescript'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-tslint                  : TypeScript linter built on tslint
//  gulp-typescript              : Handle TypeScript compilation workflow
//  tslint                       : TypeScript linter
//  typescript                   : TypeScript support tools
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Lint: TypeScript
// -------------------------------------
gulp.task('lint:typescript', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const gulpTypescript = require('gulp-typescript');
    const gulpTslint = require('gulp-tslint');
    const tslint = require('tslint');
    const typescript = require('typescript');

    gulp.src(config.paths.ts.all)
        .pipe(gulpTslint({

            // specify location of file with custom rules
            configuration: config.configs.tslint,

            // set up detailed reporting
            formatter: 'verbose'
        }))

    // display linting results in console
    .pipe(gulpTslint.report({

        // do not spit out bulk errors at end of lint
        emitError: false
    }));
});
