// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'lint:bootstrap'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//s
//  gulp                         : The streaming build system
//  gulp-bootlint                : An HTML linter for Bootstrap projects
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Lint: Bootstrap
// -------------------------------------
// TODO: setup external config file.
gulp.task('lint:bootstrap', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const gulpBootlint = require('gulp-bootlint');
    const fileIssues = [];

        return gulp.src(config.paths.html.all)
            .pipe(gulpBootlint({

                // Stops the gulp task if there are errors in the linted file.
                stoponerror: false,

                // Stops the gulp task if there are warnings in the linted file.
                stoponwarning: false,

                // Defines which log messages should be printed to stdout.
                loglevel: "debug",

                // Array of bootlint problem ID codes (as Strings) to explicitly ignore.
                disabledIds: ["performance", "test"],

                // All found issues (Objects of type LintWarning and LintError) are stored
                // in this array.
                issues: fileIssues,

                // A function that will log out the lint errors to the console.
                reportFn: function(file, lint, isError, isWarning, errorLocation) {
                    var message = (isError) ? "ERROR! - " : "WARN! - ";
                    if (errorLocation) {
                        message += file.path + " (line:" + (errorLocation.line + 1) + ", col:" + (errorLocation.column + 1) + ") [" + lint.id + "] " + lint.message;
                    } else {
                        message += file.path + ": " + lint.id + " " + lint.message;
                    }
                    console.log(message);
                },

                // A function that will log out the final lint error/warning summary to the console.
                summaryReportFn: function(file, errorCount, warningCount) {
                    if (errorCount > 0 || warningCount > 0) {
                        console.log('please fix the ${errorCount} errors and ${warningCount} warnings in ${file.path}');
                    } else {
                        console.log('No problems found in ${file.path}');
                    }
                }
            }));
});
