// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'clean:css'
//  'minify:css'
//  'move:css'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  gulp                         : The streaming build system
//  gulp-clean-css               : gulp plugin to minify CSS, using clean-css
//  gulp-rename                  : A gulp plugin to rename files easily.
//  run-sequence                 : Run a series of dependent gulp tasks in order
//
// -------------------------------------

const gulp = require('gulp');

// Import config.js
const config = require('../config');

// -------------------------------------
//   Task: Minify: CSS, Compile Sass and move to
//   destination folder
// -------------------------------------
gulp.task('minify:css', ['compile:sass'], () => {

    // Import Modules
    const runSequence = require('run-sequence');

    // Run one task at a time sequentially
    runSequence(
        'clean:css',
        'move:minified-css'
    )
});

// -------------------------------------
//   Task: Clean: CSS
// -------------------------------------
gulp.task('clean:css', () => {

    // Import Modules
    const cleanCss = require('gulp-clean-css');
    const rename = require('gulp-rename');

    return gulp.src([

        '!' + config.paths.tmp.styles + '**/*' + config.names.css.min,
        config.paths.tmp.styles + '**/*.css'
    ])

    // Minify CSS
    .pipe(cleanCss())

    // Rename CSS
    .pipe(rename({
        extname: config.names.css.min
    }))

    // Move CSS to new location
    .pipe(gulp.dest(config.paths.css.dest));
});

// -------------------------------------
//   Task: Move: CSS
// -------------------------------------
gulp.task('move:minified-css', () => {

    // Import Modules
    const runSequence = require('run-sequence');

    // Move already minified CSS
    // to the destination folder
    gulp.src(config.paths.css.entry + '**/*' + config.names.css.min)
        .pipe(gulp.dest(config.paths.css.dest))
        // .on('end', function() {
        //     setTimeout(function() {
        //         runSequence(
        //
        //             // Run task
        //             'concatinate:css'
        //         )
        //     }, 500);
        // });
});
