// *************************************
//
//   Gulpfile
//   Western & Southern
//
// *************************************
//
// Available tasks:
//  'compile:sass'
//
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
//  autoprefixer                 : Parse CSS and add vendor prefixes to CSS rules
//  gulp                         : The streaming build system
//  gulp-postcss                 : Pipe CSS through several processors, but parse CSS only once
//  gulp-sass                    : Sass plugin for Gulp to compile Sass files
//
// -------------------------------------

const gulp = require('gulp');

// -------------------------------------
//   Task: Compile: Sass, minifiy css,
//   rename css and move to destination folder
// -------------------------------------
gulp.task('compile:sass', () => {

    // Import config.js
    const config = require('../config');

    // Import Modules
    const autoprefixer = require('autoprefixer');
    const postcss = require('gulp-postcss');
    const sass = require('gulp-sass');

    const postProcessors = [
        autoprefixer({
            browsers: ['Last 2 versions']
        })
    ];

    return gulp.src(config.paths.sass.all)

    // Compile Sass
    .pipe(sass().on('error', sass.logError))

    // Run Autoprefixer on new css
    .pipe(postcss(postProcessors))

    // Move css to new location
    .pipe(gulp.dest(config.paths.tmp.styles));
});
